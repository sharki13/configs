set nocompatible
"" for gui disable toolbar
if has('gui_running')
    set guioptions-=T  "remove toolbar
    set lines=45
    set columns=180
    if has('gui_win32')
        set guifont=Powerline_Consolas:h10:cANSI
        set guifontwide=YaHei_Consolas_Hybrid:h10
        if has('directx')
            set rop=type:directx,gamma:1.0,contrast:0.5,level:1,geom:1,renmode:4,taamode:1
        endif
        "" set fancy fonts for airline
        let g:airline_powerline_fonts = 1
        set encoding=utf-8
    endif
endif

"" change lang to en_US
if $langmenu != 'en_US'
    set langmenu=en_US
    let $LANG = 'en_US'
    source $VIMRUNTIME/delmenu.vim
    source $VIMRUNTIME/menu.vim
endif

"" NeoBudnleConfig
if has('win32')
    set runtimepath^=$HOME/vimfiles/bundle/neobundle.vim/
    call neobundle#begin(expand('~/vimfiles/bundle/'))
else
    set runtimepath^=$HOME/.vim/bundle/neobundle.vim/
    call neobundle#begin(expand('~/.vim/bundle/'))
endif

NeoBundleFetch 'Shougo/neobundle.vim'

 "My Bundles here:
NeoBundle 'vim-airline/vim-airline'
NeoBundle 'vim-airline/vim-airline-themes'
NeoBundle 'matze/vim-move'
NeoBundle 'ervandew/supertab'
NeoBundle 'nathanaelkane/vim-indent-guides'
NeoBundle 'scrooloose/nerdcommenter.git'
NeoBundle 'kien/ctrlp.vim'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'jistr/vim-nerdtree-tabs'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'gustafj/vim-ttcn'
NeoBundle 'ihacklog/HiCursorWords'
NeoBundle 'tpope/vim-markdown'
NeoBundle 'terryma/vim-multiple-cursors'
NeoBundle 'Shougo/vimshell.vim'
NeoBundle 'Shougo/vimproc.vim'
NeoBundle 'jiangmiao/auto-pairs'
NeoBundle 'a.vim'


call neobundle#end()

" AirLine
set laststatus=2    " air line is visible all the time
let g:airline#extensions#tabline#enabled = 1

" Vim Move
let g:move_key_modifier = 'C'

" NERDTree
nmap <silent> <F5> :NERDTreeTabsToggle<CR>
nmap <silent> <C-F5> :NERDTreeTabsFind<CR>

"" settings
"" display
syntax on
colorscheme molokai
set number
set ruler   " show the cursor position all the time
set cursorline
set wildmenu    " show proposals when typing
set showcmd
"" tabs, indent
set autoindent  " copy indentation from previous line when starting a new line
set smartindent    " self explain
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
filetype plugin indent on
"" behavior
set backspace=indent,eol,start  " backspace works like in other apps
set nobackup
set noswapfile
set mouse=a    "  enable mouse
let mapleader = ","
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
  autocmd BufNewFile,BufReadPost *.ttcn set filetype=ttcn
  autocmd BufNewFile,BufReadPost *.md set filetype=markdown
endif
nnoremap <CR> G
nmap <C-Tab> :tabnext<cr>
nmap <Tab> :bnext<cr>
"" search
set hlsearch
set incsearch
set ignorecase   "ignore case for searching
set smartcase
map <Leader>/ :noh<cr>

"" User commands
if has('win32')
    command Settings e $HOME/_vimrc
else
    command Settings e ~/.vimrc
endif

